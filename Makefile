SOURCES=src/class-machinepack-wordpress-api.php src/machinepack-api.php src/default_config.yml

all: free premium
free: dist/machinepack-api-wordpress-plugin-free.zip
premium: dist/machinepack-api-wordpress-plugin-premium.zip

dist/machinepack-api-wordpress-plugin-free.zip: $(SOURCES) composer-prod.json composer-prod.lock
	mkdir -p dist/machinepack-api-wordpress-plugin-free
	cp -v $(SOURCES) dist/machinepack-api-wordpress-plugin-free
	zip -dgr dist/machinepack-api-wordpress-plugin-free.zip dist/machinepack-api-wordpress-plugin-free -x *.git*

dist/machinepack-api-wordpress-plugin-premium.zip: $(SOURCES) composer-prod-premium.json composer-prod-premium.lock
	mkdir -p dist/machinepack-api-wordpress-plugin-premium
	cp -v $(SOURCES) dist/machinepack-api-wordpress-plugin-premium
	COMPOSER=composer-prod-premium.json composer install --no-dev --optimize-autoloader
	zip -dgr dist/machinepack-api-wordpress-plugin-premium.zip dist/machinepack-api-wordpress-plugin-premium -x *.git*

install: composer.json
	composer install

update:
	COMPOSER=composer.json composer update
	COMPOSER=composer-prod.json composer update
	COMPOSER=composer-prod-premium.json composer update || true

clean:
	rm -rf dist/machinepack-*

composer-%.lock: composer-%.json
	COMPOSER=$? composer install --no-dev --optimize-autoloader
