# MachinePack API for Wordpress

This plugin exposes a simple JSON based API via WordPress's own WP-JSON system.

# Installation

Clone this repo, then run `make install`. Your plugin is ready to use under `dist/`
with both free and premium versions (if you have access for premium).

# Upgrading

To upgrade and add new packages, you MUST use `make update` in order to have all lock
files updated.

# Versions available

2 versions are built. One free, one premium. If you don't have access to premium
handlers, you'll only see a free plugin folder under dist.
