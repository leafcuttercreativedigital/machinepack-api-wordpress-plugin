if test ! -d /var/www/html
then
	echo 'Please only run this inside a container'
	exit 1
fi

if test ! -x /usr/local/bin/wp
then
	echo "Downloading WPCLI"
	curl -sO https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	mv wp-cli.phar /usr/local/bin/wp
	chmod +x /usr/local/bin/wp
fi

if test ! -z "$DOWNLOAD_WORDPRESS_VERSION"
then
	wp core download \
		--allow-root \
		--path="/var/www/html" \
		--force \
		--version="$DOWNLOAD_WORDPRESS_VERSION" \
		--locale="${WORDPRESS_LOCALE:-en_AU}"
fi

wp core install \
	--allow-root \
	--path="/var/www/html" \
	--url="http://localhost:8001" \
	--title="Plugin Dev WordPress image" \
	--admin_user=plugin_admin \
	--admin_password=plugin_admin \
	--admin_email=machinepack-plugin@mailinator.com \

wp rewrite structure --allow-root '/%year%/%monthnum%/%postname%'
wp rewrite flush --allow-root

if test ! -f /usr/local/etc/php/conf.d/errors_stderr.ini
then
	cat << EOCONFIG > /usr/local/etc/php/conf.d/errors_stderr.ini
display_errors=Off
error_log=/dev/stderr
log_errors=On
EOCONFIG

	apachectl restart
fi
