#!/bin/bash -eux

find src -name '*.php' \
| grep -v src/vendor \
| entr -c bash -c 'src/vendor/bin/phpcbf && src/vendor/bin/phpcs'
