<?php
/**
 * Plugin Name:     MachinePack API
 * Plugin URI:      https://leafcutter.com.au
 * Description:     Machinepack API exposes the MachinePack handlers as a JSON API via WP-JSON
 * Author:          Leafcutter Team
 * Author URI:      https://leafcutter.com.au
 * Text Domain:     machinepack-api
 * Domain Path:     /
 * Version:         1.1.1
 *
 * @package         Machinepack
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'MACHINEPACK_CONFIG' ) ) {
	define( 'MACHINEPACK_CONFIG', __DIR__ . '/default_config.yml' );
}

if ( ! defined( 'MACHINEPACK_VERSION' ) ) {
	$plugin_data = get_file_data( __FILE__, array( 'Version' => 'Version' ), false );
	define( 'MACHINEPACK_VERSION', $plugin_data['Version'] );
}

/**
 * Import MachinePack libraries
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Load API class
 *
 * @var MachinePack_WordPress_API
 */
require_once __DIR__ . '/class-machinepack-wordpress-api.php';

/**
 * Init hook for REST APIs
 */
add_action(
	'rest_api_init',
	function () {
		if ( ! defined( 'MACHINEPACK_CONFIG' ) ) {
			define( 'MACHINEPACK_CONFIG', __DIR__ . '/default_config.yml' );
		}
		$mp = new MachinePack_WordPress_API();
		$mp->register_routes();
	}
);
