<?php
/**
 * API handler class with hooks to MachinePack
 *
 * @package         Machinepack
 * @var MachinePack_WordPress_API
 */

use MachinePack\Core\MachinePack as MP;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;

/**
 * API class encapsulating MachinePack functionality
 */
class MachinePack_WordPress_API extends WP_REST_Controller {

	/**
	 * Current API version
	 *
	 * @var string
	 */
	private $version = '1.0';

	/**
	 * Vendor prefix for all REST API hooks
	 *
	 * @var string
	 */
	private $vendor = 'machinepack';

	/**
	 * Register routes for WordPress
	 *
	 * @return void
	 */
	public function register_routes() {
		foreach ( [
			'webhook',
			'send',
			'jobstatus',
		] as $route ) {
			register_rest_route(
				"{$this->vendor}/{$this->version}",
				$route,
				[
					'methods'  => WP_REST_Server::ALLMETHODS,
					'callback' => [ $this, 'execute_' . $route ],
				]
			);
		}
	}

	/**
	 * Executes a raw MP send with input data, then returns standard WP responses.
	 *
	 * @param  array|WP_REST_Request $data raw event data.
	 * @param  string                $prefix prefix to use in response messages.
	 * @return WP_REST_Response|WP_Error
	 */
	private function raw_send( $data, $prefix = '' ) {
		try {
			MP::init();

			if ( empty( $data['event'] ) || ! is_string( $data['event'] ) ) {
				return new WP_Error( $prefix . '-error', __( 'Request must include an "event" string containing the event being sent.' ) );
			}

			$results   = MP::send( $data['event'], (array) $data['data'] ?? [] );
			$http_code = 200;
			$output    = array_map(
				function ( $result ) use ( &$http_code ) {
					$http_code = max( $http_code, $result->asHttpCode() );
					return $result->asHttpResponseData();
				},
				$results
			);

			return new WP_REST_Response( $output, $http_code );
		} catch ( MissingArgumentsException $e ) {
			return new WP_Error( $prefix . '-failure', __( 'Missing event arguments.' ), explode( "\n", (string) $e ) );
		} catch ( \Throwable $e ) {
			return new WP_Error( $prefix . '-exception', __( 'Internal send API error' ), explode( "\n", (string) $e ) );
		}
	}

	/**
	 * Runs a generic hook, passing the whole request directly to the event
	 * handler
	 *
	 * @param  WP_REST_Request $request web request.
	 * @return WP_REST_Response|WP_Error
	 */
	public function execute_webhook( WP_REST_Request $request ) {
		return $this->raw_send(
			[
				'event' => 'webhook.request',
				'data'  =>
				[
					'Intangible/RESTRequest.data'   => $_REQUEST,
					'Intangible/RESTRequest.server' => $_SERVER,
				],
			],
			'webhook'
		);
	}

	/**
	 * This hook sends an event directly from request data
	 *
	 * @param  WP_REST_Request $request web request.
	 * @return WP_REST_Response|WP_Error
	 */
	public function execute_send( WP_REST_Request $request ) {
		return $this->raw_send( $request->get_params(), 'send' );
	}
}
